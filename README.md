# experiment-odata
> the goal is to have a deployable and runnable OData REST API which can be deployed to Cloud Foundry SAP for experimental purposes

## Dependencies
- Apache Maven
- Java 1.8 (Cloud Foundry SAP supports currently just 1.8 in the Trail subscribtion)

## Build
```
mvn clean package
```

## Deploy
- Manual using the [cockpit.hanatrail.ondemand.com](https://cockpit.hanatrial.ondemand.com/)
- Using the [Cloud Foundry CLI](https://docs.cloudfoundry.org/cf-cli/install-go-cli.html)
