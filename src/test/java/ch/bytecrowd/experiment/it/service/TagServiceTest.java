package ch.bytecrowd.experiment.it.service;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ch.bytecrowd.experiment.model.Tag;
import ch.bytecrowd.experiment.model.Task;
import ch.bytecrowd.experiment.repository.TagRepository;
import ch.bytecrowd.experiment.repository.TaskRepository;
import ch.bytecrowd.experiment.service.TagService;

@SpringBootTest
public class TagServiceTest {

    @Autowired
    private TagService tagService;

    @Autowired
    private TaskRepository taskRepository;
    
    @Test
    public void addTag_newTagToExistingTask_saveIsInvoked() {
        
        Task task = new Task();
        task.setTitle("TEST Task");
        task.setDescription("Lorem Ipsum ...");
        task = taskRepository.save(task);

        Tag tag_01 = new Tag();
        tag_01.setName("Le TAG");

        tagService.addTag(task, tag_01);

        List<Task> tasks = taskRepository.findAll();
        assertThat("Testing tasks count after adding", tasks.size(), is(1));
        assertThat("Test relation between task and Tags", tasks.get(0).getTags().size(), is(1));
    }
}