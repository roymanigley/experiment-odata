package ch.bytecrowd.experiment.unit.service;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.bytecrowd.experiment.model.Tag;
import ch.bytecrowd.experiment.model.Task;
import ch.bytecrowd.experiment.repository.TagRepository;
import ch.bytecrowd.experiment.repository.TaskRepository;
import ch.bytecrowd.experiment.service.TagService;

@ExtendWith(MockitoExtension.class)
public class TagServiceTest {

    @Mock
    private TagRepository tagRepo;

    @Mock
    private TaskRepository taskRepo;

    @InjectMocks
    private TagService tagService;
    
    @Test
    public void addTag_newTagToExistingTask_saveIsInvoked() {
        
        final Task task = new Task();
        task.setId(11L);
        task.setTitle("TEST Task");
        task.setDescription("Lorem Ipsum ...");

        Tag tagNew = new Tag();
        tagNew.setId(5L);
        tagNew.setName("Le new TAG");

        tagService.addTag(task, tagNew);

        verify(tagRepo).save(tagNew);
        verify(taskRepo).save(task);
    }
}