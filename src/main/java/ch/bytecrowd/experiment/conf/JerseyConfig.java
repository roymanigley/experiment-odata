package ch.bytecrowd.experiment.conf;

import javax.persistence.EntityManagerFactory;
import javax.ws.rs.ApplicationPath;

import org.apache.olingo.odata2.core.rest.ODataRootLocator;
import org.apache.olingo.odata2.core.rest.app.ODataApplication;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import ch.bytecrowd.experiment.controller.odata.TaskRootLocator;
import ch.bytecrowd.experiment.filter.EntityManagerFilter;
import ch.bytecrowd.experiment.service.TaskODataJPAServiceFactory;

@Configuration
@ApplicationPath("/api/odata")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig(TaskODataJPAServiceFactory serviceFactory, EntityManagerFactory emf) {        
        ODataApplication app = new ODataApplication();        
        app
          .getClasses()
          .forEach( c -> {
              if ( !ODataRootLocator.class.isAssignableFrom(c)) {
                  register(c);
              }
          });
         
        register(new TaskRootLocator(serviceFactory)); 
        register(new EntityManagerFilter(emf));
    }
    
}