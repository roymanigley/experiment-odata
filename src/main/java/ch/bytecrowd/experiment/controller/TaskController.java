package ch.bytecrowd.experiment.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ch.bytecrowd.experiment.model.Task;
import ch.bytecrowd.experiment.service.TaskService;

@RestController
@RequestMapping("/api/Task")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @RequestMapping(method = RequestMethod.GET, path = {"", "/"})
    public List<Task> getAll() {
        return taskService.allTasks();
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public Optional<Task> getById(@PathVariable Long id) {
        return taskService.findById(id);
    }
    
    @RequestMapping(method = RequestMethod.POST, path = {"", "/"})
    public Task postSave(@RequestBody Task task) {
        return taskService.saveTask(task);
    }
    
    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public void deleteById(@PathVariable Long id) {
        taskService.deleteTask(id);
    }
}