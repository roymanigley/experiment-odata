package ch.bytecrowd.experiment.controller.odata;

import javax.ws.rs.Path;

import org.apache.olingo.odata2.api.ODataServiceFactory;
import org.apache.olingo.odata2.core.rest.ODataRootLocator;

import ch.bytecrowd.experiment.service.TaskODataJPAServiceFactory;


@Path("/")
public class TaskRootLocator extends ODataRootLocator {
    
    private TaskODataJPAServiceFactory serviceFactory;
    
    public TaskRootLocator(TaskODataJPAServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }
 
    @Override
    public ODataServiceFactory getServiceFactory() {
       return this.serviceFactory;
    }    
}