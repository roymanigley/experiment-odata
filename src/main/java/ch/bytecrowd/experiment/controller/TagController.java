package ch.bytecrowd.experiment.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ch.bytecrowd.experiment.model.Tag;
import ch.bytecrowd.experiment.service.TagService;

@RestController
@RequestMapping("/api/Tag")
public class TagController {

    
    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }
    
    @RequestMapping(method = RequestMethod.PUT, path = {"/{idTask}", "/{idTask}/"})
    public void getById(@PathVariable Long idTask, @RequestBody Tag tag) {
        tagService.addTag(idTask, tag);
    }
}