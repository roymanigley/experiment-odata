package ch.bytecrowd.experiment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ch.bytecrowd.experiment.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    
}