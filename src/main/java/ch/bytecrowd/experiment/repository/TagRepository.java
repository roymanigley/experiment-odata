package ch.bytecrowd.experiment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ch.bytecrowd.experiment.model.Tag;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    
}