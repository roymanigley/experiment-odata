package ch.bytecrowd.experiment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.bytecrowd.experiment.model.Tag;
import ch.bytecrowd.experiment.model.Task;
import ch.bytecrowd.experiment.repository.TagRepository;
import ch.bytecrowd.experiment.repository.TaskRepository;

@Service
public class TagService {

    private final TagRepository repo;

    private TaskRepository taskRepo;
    
    @Autowired
    public TagService(TagRepository repo, TaskRepository taskRepo) {
        this.repo = repo;
        this.taskRepo = taskRepo;
    }

    public void addTag(Long taskId, Tag tag) {
        addTag(taskRepo.findById(taskId).get(), tag);
    }
    

    public void addTag(Task task, Tag tag) {
        tag.addTask(task);
        tag = repo.save(tag);
        task.addTag(tag);
        taskRepo.save(task);
    }
    
    public void removeTag(Task task, Tag tag) {
        tag.removeTask(task);
        task.removeTag(tag);
        repo.save(tag);
        taskRepo.save(task);
    }
}