package ch.bytecrowd.experiment.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import ch.bytecrowd.experiment.model.Task;
import ch.bytecrowd.experiment.repository.TaskRepository;

@Service
public class TaskService {

    private final TaskRepository repository; 

    public TaskService(TaskRepository repository) {
        this.repository = repository;
    }

    public List<Task> allTasks() {
        return repository.findAll();
    } 
    
    public Optional<Task> findById(Long id) {
        return repository.findById(id);
    } 
    
    public Task saveTask(Task task) {
        return repository.save(task);
    }

    public void deleteTask(Long id) {
        repository.deleteById(id);
    }
}